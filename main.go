package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/Shopify/sarama"
	"github.com/tjarratt/babble"
)

var (
	run            bool   = true
	produced       int    = 0
	brokers        string = ""
	version        string = ""
	topic          string = ""
	acknowledge    bool   = true
	verbose        bool   = false
	message        string = ""
	wait           string = ""
	compression    int    = 0
	randommsg      bool   = false
	randommsgwords int    = 1
)

func setupCloseHandler() {
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		run = false
		fmt.Println("terminating via signal")
		fmt.Printf("Produced %v messages\n", produced)
		os.Exit(0)
	}()
}

func init() {
	flag.StringVar(&brokers, "brokers", "", "Kafka bootstrap brokers to connect to, as a comma separated list")
	flag.StringVar(&message, "message", "", "Message to send to Kafka")
	flag.StringVar(&wait, "wait", "0ms", "Waiting time when producing messages to Kafka (ex: 100ms, 3s, 10ns")
	flag.StringVar(&version, "version", "1.1.1", "Kafka cluster version")
	flag.StringVar(&topic, "topic", "", "Kafka topics to produce on")
	flag.BoolVar(&verbose, "verbose", false, "Sarama logging")
	flag.BoolVar(&randommsg, "randommsg", false, "Generate random messages (words)")
	flag.IntVar(&randommsgwords, "randommsgwords", 1, "Number of words to generate (must be >= 1)")
	flag.IntVar(&compression, "compression", 0, "Enable compression: 0- no compression (default), 1- gzip, 2- snappy, 3- lz4, 4- zstd")
	flag.Parse()

	if len(brokers) == 0 {
		log.Fatalln("No Kafka bootstrap brokers defined, please set the -brokers flag")
	}

	if len(topic) == 0 {
		log.Fatalln("No topic given to produce, please set the -topic flag")
	}

	if randommsg == false {
		if len(message) == 0 {
			log.Fatalln("No message given, please set the -message flag")
		}
	}

	if randommsgwords < 1 {
		randommsgwords = 1
	}
}

func getCompression(c int) sarama.CompressionCodec {
	if c == 1 {
		return sarama.CompressionGZIP
	}
	if c == 2 {
		return sarama.CompressionSnappy
	}
	if c == 3 {
		return sarama.CompressionLZ4
	}
	if c == 4 {
		return sarama.CompressionZSTD
	}

	return sarama.CompressionNone
}

func main() {
	fmt.Println("Starting a new Sarama producer...")

	setupCloseHandler()

	version, err := sarama.ParseKafkaVersion(version)
	if err != nil {
		log.Panicf("Error parsing Kafka version: %v", err)
	}

	if verbose {
		sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)
	}

	config := sarama.NewConfig()
	config.Version = version
	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true
	config.Producer.Retry.Max = 10
	config.Producer.Retry.Backoff = 1 * time.Second
	config.Producer.RequiredAcks = sarama.WaitForLocal
	config.Metadata.RefreshFrequency = 5 * time.Second
	config.Producer.Compression = getCompression(compression)

	log.Printf("Using compression: %v\n", getCompression(compression))

	client, err := sarama.NewClient(strings.Split(brokers, ","), config)
	if err != nil {
		log.Fatalf("Cannot create a client: %v\n", err)
	}
	defer func() {
		if err := client.Close(); err != nil {
			log.Printf("Failed to close client: %v\n", err)
		}
	}()

	producer, err := sarama.NewSyncProducerFromClient(client)
	if err != nil {
		log.Fatalf("Cannot create a producer: %v\n", err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			log.Printf("Failed to close producer: %v\n", err)
		}
	}()

	var m sarama.ProducerMessage
	for run {
		if randommsg {
			b := babble.NewBabbler()
			b.Count = randommsgwords
			b.Separator = " "

			msg := sarama.ProducerMessage{
				Topic: topic,
				Key:   nil,
				Value: sarama.StringEncoder(b.Babble()),
			}
			m = msg
		} else {
			msg := sarama.ProducerMessage{
				Topic: topic,
				Key:   nil,
				Value: sarama.StringEncoder(message),
			}
			m = msg
		}
		p, o, err := producer.SendMessage(&m)
		produced++
		if err != nil {
			log.Printf("Error sending message: %v\n", err)
		} else {
			log.Printf("Producing message to partition: %v, offset: %v\n", p, o)
		}

		w, err := time.ParseDuration(wait)
		if err != nil {
			log.Fatalf("Error parsing duration: %v", err)
		}
		time.Sleep(w)
	}
}
