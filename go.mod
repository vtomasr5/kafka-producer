module gitlab.com/vtomasr5/kafka-producer

go 1.13

require (
	github.com/Shopify/sarama v1.24.0
	github.com/klauspost/cpuid v1.2.1 // indirect
	github.com/tjarratt/babble v0.0.0-20191209142150-eecdf8c2339d
)
